<?php

function getEmbed($unparsed_url) {

	parse_str( parse_url( $unparsed_url, PHP_URL_QUERY ), $result );
	
	$embed_link = "http://www.youtube.com/embed/" . $result['v'];
	$player_link = '<iframe id="ytplayer" type="text/html" src="' . $embed_link . '" frameborder="0"></iframe>';
	
	return $player_link;
	
}

$url = "https://www.youtube.com/watch?v=UPuXvpkOLmM";
echo getEmbed($url);

?> 
